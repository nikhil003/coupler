#############################################
#           WRITTEN BY : NIKHIL GARG        #
#           FOR : DHI MIKE OUTPUT           #
#      WORKS ONLY FOR : WINDOWS MACHINE     #
#############################################

from function_libraries import sys, os, glob, json
from function_libraries import Dataset, MFDataset
from function_libraries import datetime
from function_libraries import ravel

#===============================================================================
nargs = len(sys.argv)
if nargs < 2:
    print(" <start date>            \n\
            <time step size>        \n\
        ")
    sys.exit(-1)
#===============================================================================

import clr
from clr import System
from System import Reflection, Array, Single

"""
reads json file to find some settings, which are needed if running Ideal simulation
This was an easy fix thats why I did it this way. Can be more cleaner.
"""
with open(os.path.join(os.getcwd(), 'model_config.json'), 'r') as jsonFile:
    configFile = json.load(jsonFile)

wrf_dir = configFile['WindowsPath']['WRF']

wrf_file = MFDataset(glob(os.path.join(wrf_dir, 'wrfout*')))

#get list of dll in directory
dll_list = glob(os.path.join(os.getcwd(), '*.dll'))
dll_list.sort()

for dllName in dll_list:
    Reflection.Assembly.LoadFile(dllName)

try:
    import DHI
    from DHI.Generic.MikeZero import *
    from DHI.Generic.MikeZero.DFS.dfs123 import *
    from DHI.Generic.MikeZero.DFS import * 
    from DHI.Generic.MikeZero import DFS
    dhi_modules = 1
except ImportError:
    print("Could Not import DHI SDK modules")
    print("Check whether MIKE SDK and Python Dot NET libraries are installed correctly")
    dhi_modules = 0


if dhi_modules == 1:
    filename = os.path.join(configFile['WindowsPath']['MIKE'], 'wind_input.dfs2')
    factory = DFS.DfsFactory()
    builder = Dfs2Builder.Create('Python dfs2 file', 'Python DFS', 0)

    nx = wrf_file.variables['U10'][:].shape[2]
    ny = wrf_file.variables['U10'][:].shape[1]

    lon_min = wrf_file.variables['XLONG'][0,0,0]
    lon_max = wrf_file.variables['XLONG'][0,0,-1]
    lat_min = wrf_file.variables['XLAT'][0,0,0]
    lat_max = wrf_file.variables['XLAT'][0,-1,0]
    if configFile['RunType']['WRF'] == 'real':
        delta_x = (lon_max - lon_min) / float(nx)
        delta_y = (lat_max - lat_min) / float(ny)
    elif configFile['RunType']['WRF'] == 'ideal':
        delta_x = float(configFile['Grid']['StepSize'])
        delta_y = float(configFile['Grid']['StepSize'])

    builder.SetDataType(1)
    #builder.SetGeographicalProjection(factory.CreateProjectionUndefined())
    builder.SetGeographicalProjection(factory.CreateProjectionProjOrigin('LONG/LAT', \
        lon_min, lat_min, 0))
    myDate = datetime.strptime(sys.argv[1], '%Y-%m-%d_%H_%M_%S')

    builder.SetTemporalAxis(factory.CreateTemporalEqCalendarAxis(eumUnit.eumUsec, \
        System.DateTime(myDate.year, myDate.month, myDate.day, \
        myDate.hour, myDate.minute ,myDate.second), 0, int(float(sys.argv[2]))))

    builder.SetSpatialAxis(factory.CreateAxisEqD2(eumUnit.eumUmeter, nx, 0, \
        delta_x, ny, 0, delta_y))
    builder.DeleteValueFloat = -1e-30

    builder.AddDynamicItem('U Wind Speed U m/s', \
        eumQuantity.Create(eumItem.eumIWindSpeed, eumUnit.eumUmeterPerSec), \
        DFS.DfsSimpleType.Float, DFS.DataValueType.Instantaneous)
    builder.AddDynamicItem('V Wind Speed V m/s', \
        eumQuantity.Create(eumItem.eumIWindSpeed, eumUnit.eumUmeterPerSec), \
        DFS.DfsSimpleType.Float, DFS.DataValueType.Instantaneous)
    #builder.AddDynamicItem('Q Flux m^3/s/m', eumQuantity.Create(eumItem.eumIFlowFlux, \
    #   eumUnit.eumUm3PerSecPerM), DfsSimpleType.Float, DataValueType.Instantaneous)

    builder.CreateFile(filename)
    dfs = builder.GetFile()


    times = wrf_file.variables['U10'][:].shape[0]
    for i in xrange(times):
        data1 = wrf_file.variables['U10'][i,:,:]
        data2 = wrf_file.variables['V10'][i,:,:]
    #   data3 = (10*np.sin(X*(2*np.pi/1000) + np.pi/2 -i*2*np.pi/25)*np.cos(Y*(np.pi/1000)))
        dfs.WriteItemTimeStepNext(0, Array[Single](ravel(data1[:])))
        dfs.WriteItemTimeStepNext(0, Array[Single](ravel(data2[:])))
    #   dfs.WriteItemTimeStepNext(0, Array[Single](np.ravel(data3[:])))
    dfs.Close();
    jsonFile.close()
else:
    print("Could Not make MIKE forcing Files")
    print("Check and Try again")