from function_libraries import floor

def WRFnamelist(inputfile=None, outputfile=None, timestamps=None, TimeStep=None, real_run = False):
    """
    This function is designed for WRF namelist operations. 
    It will modify the input namelist with the time values
    specified in the model config
    """
    nmin = inputfile
    nmout = outputfile 
    nt = TimeStep
    for line in nmin.readlines():
        line = line.strip()
        if real_run is False:
            delta = timestamps[nt+1] - timestamps[nt]
        else:
            delta = timestamps[-1] - timestamps[0]
        run_hours =  int(floor(delta.seconds / 3600.)) 
        run_minutes = ((delta.seconds /3600. - run_hours) * 3600. / 60.0)
        run_seconds = delta.seconds - int(floor(run_minutes)) * 60 - run_hours * 3600

        if line.startswith('run_days'):
            print >> nmout, " run_days                            = %d," % delta.days
        elif line.startswith('run_hours'):
            print >> nmout, " run_hours                           = %d," % run_hours
        elif line.startswith('run_minutes'):
            print >> nmout, " run_minutes                         = %02d," % run_minutes
        elif line.startswith('run_seconds'):
            print >> nmout, " run_seconds                         = %02d," % run_seconds
        #here is the time steps being written
        #start time information
        elif line.startswith('start_year'):
            print >> nmout, " start_year                          = %d," % timestamps[nt].year
        elif line.startswith('start_month'):
            print >> nmout, " start_month                         = %02d," % timestamps[nt].month
        elif line.startswith('start_day'):
            print >> nmout, " start_day                           = %02d," % timestamps[nt].day
        elif line.startswith('start_hour'):
            print >> nmout, " start_hour                          = %02d," % timestamps[nt].hour
        elif line.startswith('start_minute'):
            print >> nmout, " start_minute                        = %02d," % timestamps[nt].minute
        elif line.startswith('start_second'):
            print >> nmout, " start_second                        = %02d," % timestamps[nt].second
        #end date information
        elif line.startswith('end_year'):
            if real_run is False:
                print >> nmout, " end_year                            = %d," % timestamps[nt+1].year
            else:
                print >> nmout, " end_year                            = %d," % timestamps[-1].year
        elif line.startswith('end_month'):
            if real_run is False:
                print >> nmout, " end_month                           = %02d," % timestamps[nt+1].month
            else:
                print >> nmout, " end_month                           = %02d," % timestamps[-1].month
        elif line.startswith('end_day'):
            if real_run is False:
                print >> nmout, " end_day                             = %02d," % timestamps[nt+1].day
            else:
                print >> nmout, " end_day                             = %02d," % timestamps[-1].day
        elif line.startswith('end_hour'):
            if real_run is False:
                print >> nmout, " end_hour                            = %02d," % timestamps[nt+1].hour
            else:
                print >> nmout, " end_hour                            = %02d," % timestamps[-1].hour
        elif line.startswith('end_minute'):
            if real_run is False:
                print >> nmout, " end_minute                          = %02d," % timestamps[nt+1].minute
            else:
                print >> nmout, " end_minute                          = %02d," % timestamps[-1].minute
        elif line.startswith('end_second'):
            if real_run is False:
                print >> nmout, " end_second                          = %02d," % timestamps[nt+1].second
            else:
                print >> nmout, " end_second                          = %02d," % timestamps[-1].second
        #restart information
        elif line.startswith('restart'):
            if line.split()[0] == 'restart_interval':
                print >> nmout, " restart_interval                    = %02d," % ((timestamps[nt+1] - timestamps[nt]).seconds / 60)
            else:
                if nt == 0:
                    print >> nmout, " restart                             = .false.,"               
                else:
                    print >> nmout, " restart                             = .true.,"
        elif line.startswith('&') or line.startswith('/'):
            print >> nmout, line
        else:
            print >> nmout, " " + line
    return