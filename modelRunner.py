from function_libraries import os, subprocess, sys

def run_model(model_name = None, path_to_dir=None):
    """
    This function is called when running a model, for example 
    when executing wrf.exe or real.exe or even FemEngineSWMPI.exe
    It needs two input, path where the executable is located and the name of 
    the executable
    """
    try:
        from subprocess import DEVNULL
    except ImportError:
        DEVNULL = open(os.devnull, 'wb')
    if (model_name is None) or (path_to_dir is None):
        print("Specify both model_name and path_to_dir, incorrect arguments")
    else:
        if os.path.isfile(os.path.join(path_to_dir, model_name)):
            with open(os.path.join(os.getcwd(), 'model_out.log'), 'w') as out, \
                open(os.path.join(os.getcwd(), 'model_error.log'), 'w') as err:
                p = subprocess.Popen(os.path.join(path_to_dir, model_name), cwd=path_to_dir, \
                stdin = subprocess.PIPE, stdout = out, stderr = err)
    p.communicate()
    out.close()
    err.close()