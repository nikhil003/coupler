"""

written by : nikhil
purpose: 
1. Interpolate values from Element centre to Nodes
2. Interpolate from MIKE unstructured grid nodes to
   WRF structured grid
3. Output in HDF5 format

import python net to handle DHI stuff

"""
from function_libraries import os, sys, glob, json
from function_libraries import Dataset, h5py
from function_libraries import fromiter, sqrt, empty, zeros, pi, power, divide, \
    add, multiply, reshape, array

try:
    from remap import remap
    from ctypes import c_double, c_float
    use_cython = True
except ImportError:
    use_cython = False

if os.name == 'nt':
    import clr
    from clr import System
    from System import Reflection, Array, Single
"""
get list of dll in directory
"""
dll_list = glob(os.path.join(os.getcwd(), '*.dll'))
dll_list.sort()
dll_list = dll_list[:-1]

for dllName in dll_list:
    Reflection.Assembly.LoadFile(dllName)

#import DHI module
import DHI
from DHI.Generic.MikeZero import *
from DHI.Generic.MikeZero.DFS import *
"""

user customization if necessary

"""
nargs = len(sys.argv)
if nargs < 2:
    print " <wrf windows directory> \n\
            <mike windows directory>\n\
        "
    sys.exit(-1)    
wrf_dir = sys.argv[1]
mike_path = sys.argv[2]
#print os.path.join(wrf_dir,'archive','wrfout_d01_2014-06-28_00_00_00')
#print os.path.join(mike_path[1:], 'wave_params.dfsu')
print wrf_dir
try:
    wrf_file = Dataset(glob(os.path.join(wrf_dir[1:], 'wrfout*'))[0])
except IndexError:
    wrf_file = Dataset(glob(os.path.join(wrf_dir, 'wrfout*'))[0])
try:
    mike_output = DfsFileFactory.DfsuFileOpen(os.path.join(mike_path[1:], 'wave_params.dfsu'))
except IndexError:
    mike_output = DfsFileFactory.DfsuFileOpen(os.path.join(mike_path, 'wave_params.dfsu'))

extractNames = 1
"""

to extract variable names, I have used DfsGenericOpen.
I am not sure why it doesn't work with other file open command.

"""
if extractNames == 1:
    from DHI.Generic.MikeZero.DFS.dfs0 import *

    temp_file = DfsFileFactory.DfsGenericOpen(os.path.join(mike_path[1:], 'wave_params.dfsu'))
    nvars = temp_file.ItemInfo.Items.Count
    filevars = zeros(shape = (nvars, 3), dtype = object)
    for i in xrange(nvars):
        filevars[i, 0] = temp_file.ItemInfo.Items[i].Name
        filevars[i, 1] = temp_file.ItemInfo.Items[i].Quantity.Unit
        filevars[i, 2] = temp_file.ItemInfo.Items[i].Quantity.UnitAbbreviation
    temp_file.Close()
    del(temp_file)
    
gridx = fromiter(mike_output.X, dtype=float)
gridy = fromiter(mike_output.Y, dtype=float)

with open(os.path.join(os.getcwd(), 'model_config.json'),'r') as jsonFile:
    configFile = json.load(jsonFile)

extract_vars = configFile['Vars']['Varnum']

mapVars = {}
for var in extract_vars:
    if filevars[var-1, 0] == 'Sign. Wave Height':
        mapVars[var] = 'hwave'
    elif filevars[var-1, 0] == 'Peak Wave Period':
        mapVars[var] = 'twave'
    elif filevars[var-1, 0] == 'x-comp. of wave vector':
        mapVars[var] = 'uvel'
    elif filevars[var-1, 0] == 'y-comp. of wave vector':
        mapVars[var] = 'vvel'
    elif filevars[var-1, 0] == 'Wind friction speed':
        mapVars[var] = 'ustar'
    elif filevars[var-1, 0] == 'Roughness length':
        mapVars[var] = 'z0wave'

mike_data = {}
for var in extract_vars:
    mike_data[mapVars[var]] = fromiter(mike_output.ReadItemTimeStep(var, 1).Data, dtype=float)

wave_velocity = sqrt(add(power(mike_data['uvel'][:], 2), power(mike_data['vvel'][:], 2)))
mike_data['lwave'] = divide(multiply(9.81, power(mike_data['twave'][:], 2)), (2. * pi))

del(wave_velocity)
if os.path.isfile(os.path.join(os.getcwd(), 'namelist', 'remap_wghts_MIKE_to_WRF.nc')) == True:
    if configFile['remapping_lib'] == 'SCRIP':
        remapfile = Dataset(os.path.join(os.getcwd(), 'namelist', 'remap_wghts_MIKE_to_WRF.nc'), 'r')
        src_grid_size = remapfile.variables['src_grid_center_lat'].shape
        dst_grid_size = remapfile.variables['dst_grid_center_lat'].shape
        remap_matrix = remapfile.variables['remap_matrix'][:]
        num_wgts = remap_matrix.shape[1]
        num_links = remap_matrix.shape[0]
        src_address = remapfile.variables['src_address'][:]
        dst_address = remapfile.variables['dst_address'][:]
        data_out = zeros(shape = (len(var_name),dst_grid_size[0]), dtype=float)

    elif configFile['remapping_lib'] == 'ESMF':
        remapfile = Dataset(os.path.join(os.getcwd(), 'namelist', 'remap_wghts_MIKE_to_WRF.nc'), 'r')
        nx = remapfile.variables['xc_b'].shape[0]
        num_links = remapfile.variables['row'].shape[0]
        data_out = zeros(shape = (len(mike_data.keys()), nx), dtype=float)
        dst_address = remapfile.variables['row'][:]
        src_address = remapfile.variables['col'][:]
        remap_matrix = remapfile.variables['S'][:].astype(c_float)

    for j in xrange(len(mike_data.keys())):
            if (use_cython == True) and configFile['remapping_lib'] == 'SCRIP':
                dst_address = dst_address + 1
                data_out[j, :] = remap(dst_grid_size[0], num_links, \
                array(remap_matrix[:,0], dtype=c_float), src_address, dst_address, \
                array(mike_data[mike_data.keys()[j]], dtype=c_float))
            elif (use_cython == True) and configFile['remapping_lib'] == 'ESMF':
                data_out[j, :] = remap(nx, num_links, \
                array(remap_matrix, dtype=c_float), src_address, dst_address, \
                array(mike_data[mike_data.keys()[j]], dtype=c_float))
            else:
                for i in xrange(num_links):
                    data_out[j, dst_address[i] - 1] = data_out[var, dst_address[i] - 1] + \
                    remap_matrix[i,0] * mike_data[mike_data[mike_data.keys()[j]]][src_address[i] - 1]

    data_out = reshape(data_out, (len(mike_data.keys()), \
        wrf_file.variables['XLONG'].shape[1], wrf_file.variables['XLONG'].shape[2]))

    """
    output to .h5 file
    """
    wrf_file.close()
    mike_output.Close()
    remapfile.close()
    outFile = h5py.File(os.path.join(os.getcwd(), 'namelist', 'mike_data.h5'), 'w');
    for i in xrange(len(mike_data.keys())):
        outFile.create_dataset(mike_data.keys()[i], data=data_out[i, :, :])
    outFile.close()
else:
    print("Check the path of the remap file")
    print("Exiting, Bye")
