"""
written by : nikhil                        
purpose:WRF output to MIKE wind input
      WORKS ONLY FOR : WINDOWS MACHINE     
"""
from function_libraries import os, sys, glob, json
from function_libraries import Dataset, ravel

if os.name == 'nt':
    import clr
    from clr import System
    from System import Reflection, Array, Single

nargs = len(sys.argv)

#====================================================
if nargs < 1:
    print(" <wrf file name>         \n\
        ")
    sys.exit(-1)    
#====================================================
#get list of dll in directory
dll_list = glob(os.path.join(os.getcwd(), '*.dll'))
dll_list.sort()

for dllName in dll_list:
    Reflection.Assembly.LoadFile(dllName)
try:
    import DHI
    from DHI.Generic.MikeZero import *
    from DHI.Generic.MikeZero.DFS.dfs123 import *
    from DHI.Generic.MikeZero.DFS import *
    from DHI.Generic.MikeZero import DFS
    dhi_modules = 1

except ImportError:
    print("Could Not Import DHI SDK modules")
    dhi_modules = 0

with open(os.path.join(os.getcwd(), 'model_config.json'),'r') as jsonFile:
    configFile = json.load(jsonFile)

if dhi_modules == 1:
    if os.path.isfile(os.path.join(configFile['WindowsPath']['WRF'], sys.argv[1])) is True:
        wrf_file = Dataset(os.path.join(configFile['WindowsPath']['WRF'], sys.argv[1]))
    else:
        print("Unable to locate file %r"% sys.argv[1])

    filename = 'wind_input.dfs2'

    mike_out = DFS.DfsFileFactory.Dfs2FileOpenAppend(os.path.join(configFile['WindowsPath']['MIKE'], filename))

    u10 = wrf_file.variables['U10'][0,:,:]
    v10 = wrf_file.variables['V10'][0,:,:]

    mike_out.WriteItemTimeStepNext(0, Array[Single](ravel(u10[:])))
    mike_out.WriteItemTimeStepNext(0, Array[Single](ravel(v10[:])))

    mike_out.Close()
else:
    print("""Check the DHI modules installation and
    installation of Python Dot Net libraries and MIKE SDK""")