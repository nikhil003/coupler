from function_libraries import floor, datetime

def MIKEnamelist(inputFile=None, outputFile=None, timeStamps=None, TimeStep=None):
    """
    This function is designed for MIKE PFS file operations. 
    It will modify the input pfs file with the time values
    specified in the model config
    """
    pfsin = inputFile
    pfsout = outputFile
    nt = TimeStep
    pfsin.seek(0,0)

    for i, line in enumerate(pfsin, 0):
        if '[INITIAL_CONDITIONS]' in line:
            num = i

    num += 2
    pfsin.seek(0,0)

    for i, line in enumerate(pfsin, 0):
        templine = line.strip()
        delta = timeStamps[nt+1] - timeStamps[nt]
        run_hours =  int(floor(delta.seconds / 3600.)) 
        run_minutes = ((delta.seconds /3600. - run_hours) * 3600. / 60.0)
        run_seconds = delta.seconds - int(floor(run_minutes)) * 60 - run_hours * 3600
        if templine.startswith("start_time"):
            if i < 70:
                print >> pfsout, "      start_time = %s" % datetime.strftime(timeStamps[nt], "%Y,%m,%d,%H,%M,%S")
            else:
                print >> pfsout, line[:-1]
        elif templine.startswith("time_step_interval"):
            print >> pfsout, "      time_step_interval = %d" % delta.seconds
        elif templine.startswith("number_of_time_steps"):
            print >> pfsout, "      number_of_time_steps = 1"
        elif templine.startswith("file_name_A"):
            if TimeStep == 0:
                print >> pfsout, "         file_name_A = ||"
            else:
                print >> pfsout, "         file_name_A = |.\hotfile.dfsu|"
        elif i == num:
            if TimeStep == 0:
                print >> pfsout, "         type = 0"
            else:
                print >> pfsout, "         type = 2"
        else:
            print >> pfsout, line[:-1]
    return
