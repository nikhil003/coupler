#!/usr/bin/python

from function_libraries import os, sys, json, subprocess, glob, shutil, time
from function_libraries import floor, datetime, timedelta
import modelRunner, modelConfig
import add_var_met, add_var2wrfrst
import timeManage

start = """
       ____________________________________________________
      |                                                    |
      |Coupler Script to run WRF and MIKE model in         |
      |sequential configuration                            |
      |                                                    |
      |written by : Nikhil                                 |
      |                                                    |
      |____________________________________________________|
      
       ____________________________________________________
      |                                                    |
      |This is the main coupler file                       |
      |It requires model_config.json file to run the model |
      |                                                    |
      |____________________________________________________|
      
      """

print(start)

def move_file(source_path = None, destination_path=None, file_name = None):
    """
    A thin wrapper to copy a file from source directory to 
    a destination directory
    """
    if (source_path is None) or (destination_path is None) or (file_name is None):
        print("""
        Aguments source_path, destination_path and file_name must be provided
        """)
    else:
        if os.path.isdir(source_path):
            if os.path.isdir(destination_path):
                if os.path.isfile(os.path.join(source_path, file_name)):
                    shutil.move(os.path.join(source_path, file_name), \
                    os.path.join(destination_path, file_name))
                else:
                    print("""
                    Source file for moving doesnt exist at the specified location
                    """)
            else:
                print("""
                Incorrect value for destination directory for moving file
                """)
        else:
            print("""
            Incorrect value for source directory for moving file
            """)

def remove_file(file_name = None, path_to_file=None):
    """
    A simple function to remove a file after copied to 
    desired directory
    """
    if (file_name is None) or (path_to_file is None):
        print("""
        Both file_name and path_to_file has to be specified
        """)
    else:
        if os.path.isfile(os.path.join(path_to_file, file_name)):
            os.remove(os.path.join(path_to_file, file_name))
        else:
            print("""
            Incorrect path_to_file or file_name argument, check the 
            file name or the path and try again
            """)

def make_symbolic_link(source_path = None, destination_path=None, file_name = None):
    """
    A wrapper for os.symlink function from python default libraries
    """
    if (source_path is None) or (destination_path is None) or (file_name is None):
        print("""
        Arguments source_path, destination_path and file_name must be provided
        """)
    else:
        if len(file_name) == 35:
            if os.path.isfile(os.path.join(source_path, file_name)):
                if os.path.isdir(destination_path):
                    """
                    this step also check if a link with same name exists, if so 
                    then it unlinks the earlier link and makes a new link
                    """
                    if os.path.islink(os.path.join(source_path, file_name), \
                        os.path.join(destination_path, file_name)):
                        os.unlink(os.path.join(destination_path, file_name))
                    """
                    makes a new link here
                    """
                    os.symlink(os.path.join(source_path, file_name), \
                        os.path.join(destination_path, file_name))
                else:
                    print("""
                    Incorrect destination_path argument for making symbolic link
                    """)
            else:
                print("""
                Source File for making symbolic link doesnt not exist
                """)
        else:
            """
            if length of file name is longer than 35, it is assumed that the path 
            is included in the name. This step also check if previous link exist, 
            if so it unlinks the exisiting link and makes a new link
            """
            if os.path.islink(os.path.join(destination_path, file_name[-33:])):
                os.unlink(os.path.join(destination_path, file_name[-33:]))
            os.symlink(file_name, \
                os.path.join(destination_path, file_name[-33:]))


if os.path.isfile(os.path.join(os.getcwd(), 'model_config')) == True:
    run_coupler = 1
    if os.path.isfile(os.path.join(os.getcwd(), 'model_config.json')) == True:
        os.remove(os.path.join(os.getcwd(), 'model_config.json'))
#
    modelConfig.makeJson(os.path.join(os.getcwd(), 'model_config'))
    with open(os.path.join(os.getcwd(), 'model_config.json')) as configFile:
        modelSetup = json.load(configFile)
    """
    modifying the flag so that the model is capable of running 
    uncoupled atmosphere or wave model
    """
    if modelSetup['Coupling']['atm2wave'] == 0 and \
            modelSetup['Coupling']['wave2atm'] == 0 and \
            int(modelSetup['Model']['atmosphere']) == 1:
            run_coupler = 0
    elif modelSetup['Coupling']['atm2wave'] == 0 and \
            modelSetup['Coupling']['wave2atm'] == 0 and \
            int(modelSetup['Model']['wave']) == 1:
            run_coupler = 0
else:
    run_coupler = 99
    print("""Unable to Locate 'model_config' file in the directory, 
    check the path and try again""")

#print("run_coupler flag = %d"% run_coupler)
if os.path.isdir(os.path.join(modelSetup['LogPath']['WRF'])) == False:
    os.mkdir(os.path.join(modelSetup['LogPath']['WRF']))
    
if os.path.isdir(os.path.join(modelSetup['LogPath']['MIKE'])) == False:
    os.mkdir(os.path.join(modelSetup['LogPath']['MIKE']))
    
if os.path.isfile(os.path.join(modelSetup['LogPath']['WRF'], 'couplerLog.log')) == True:
    remove_file(file_name = 'couplerLog.log', path_to_file = os.path.join(modelSetup['LogPath']['WRF']))
    
coupler_log = open(os.path.join(modelSetup['LogPath']['WRF'], 'couplerLog.log'), 'a')

if run_coupler == 1:

    TimeStamps = timeManage.getTimestamps( \
    sT = datetime.strptime(modelSetup['Time']['start'], "%Y:%m:%d_%H:%M:%S"), \
    eT = datetime.strptime(modelSetup['Time']['end'], "%Y:%m:%d_%H:%M:%S"), \
    min = datetime.strptime(modelSetup['Time']['step'], "%H:%M:%S").minute, \
    hrs = datetime.strptime(modelSetup['Time']['step'], "%H:%M:%S").hour, \
    sec = datetime.strptime(modelSetup['Time']['step'], "%H:%M:%S").second)

    if os.path.isdir(modelSetup['Archive']['WRF']) == False:
        os.mkdir(modelSetup['Archive']['WRF'])

    if os.path.isdir(modelSetup['Archive']['MIKE']) == False:
        os.mkdir(modelSetup['Archive']['MIKE'])
        
    for nt in xrange(len(TimeStamps)-1):
#    for nt in xrange(2):
        start = time.time()
        #make symbolic link of tslist file
        if os.path.islink(os.path.join(modelSetup['UnixPath']['WRF'], 'tslist')):
            os.unlink(os.path.join(modelSetup['UnixPath']['WRF'], 'tslist'))
        shutil.copyfile(os.path.join(modelSetup['Namelist']['WRF'], 'tslist'), \
            os.path.join(modelSetup['UnixPath']['WRF'], 'tslist'))
        if nt == 0:
            print("""
            Adding Variable to Metgrid files
            """)
            
            add_var_met.main(modelSetup['InputData']['metgrid'])
            metgrid_file_list = glob(os.path.join(modelSetup['InputData']['metgrid'], \
            'met_em*'))
            for metgrid_file in metgrid_file_list:
                make_symbolic_link(source_path = modelSetup['InputData']['metgrid'], \
                    destination_path = modelSetup['UnixPath']['WRF'], \
                    file_name = metgrid_file)
#
        timeManage.main(modelConfig = modelSetup, TimeStamps = TimeStamps, Nstep = nt)
##      
        wrf_preprocess_time = time.time()
        coupler_log.write('WRF Preprocessing %d'%nt + ' %4.2f'%(wrf_preprocess_time - start) + ' seconds' + '\n')
        coupler_log.flush()
#
        print("\
            Running WRF for timestep %r"%str(nt)+" \
        ")
        wrf_start = time.time()
#
        modelRunner.run_model(model_name = 'wrf.exe', path_to_dir = modelSetup['UnixPath']['WRF'])
#
        wrf_end = time.time()
        coupler_log.write('WRF Runtime %d'%nt + ' %4.2f'%(wrf_end - wrf_start) + ' seconds' + '\n')
        coupler_log.flush()
        
        if os.path.isfile(os.path.join(os.getcwd(), 'model_out.log')):
            shutil.move(os.path.join(os.getcwd(), 'model_out.log'), \
            os.path.join(modelSetup['LogPath']['WRF'], \
            'wrfout_'+TimeStamps[nt].strftime("%Y-%m-%d_%H_%M_%S")+'.log'))
        if os.path.isfile(os.path.join(os.getcwd(), 'model_error.log')):
            shutil.move(os.path.join(os.getcwd(), 'model_error.log'), \
            os.path.join(modelSetup['LogPath']['WRF'], \
            'wrferror_'+TimeStamps[nt].strftime("%Y-%m-%d_%H_%M_%S")+'.log'))
        if nt == 0:
            move_file(source_path = modelSetup['UnixPath']['WRF'], \
            destination_path = modelSetup['Archive']['WRF'], \
            file_name = "wrfout_d01_"+TimeStamps[nt].strftime("%Y-%m-%d_%H_%M_%S"))

        move_file(source_path = modelSetup['UnixPath']['WRF'], \
        destination_path = modelSetup['Archive']['WRF'], \
        file_name = "wrfout_d01_"+TimeStamps[nt+1].strftime("%Y-%m-%d_%H_%M_%S"))

        if os.path.isdir(os.path.join(modelSetup['Archive']['WRF'], \
                'wrf_ts_'+TimeStamps[nt].strftime("%Y-%m-%d_%H_%M_%S"))) is False:
            os.mkdir(os.path.join(modelSetup['Archive']['WRF'], \
                'wrf_ts_'+TimeStamps[nt].strftime("%Y-%m-%d_%H_%M_%S")))
##
        for fileName in glob(os.path.join(modelSetup['UnixPath']['WRF'], '*.TS')):
            move_file(source_path=modelSetup['UnixPath']['WRF'], \
            destination_path=os.path.join(modelSetup['Archive']['WRF'], \
                'wrf_ts_'+TimeStamps[nt].strftime("%Y-%m-%d_%H_%M_%S")), \
                file_name=fileName[len(modelSetup['UnixPath']['WRF'])+1:])
##
        """
        Coupling step from atmosphere to wave
        """
        if modelSetup['Coupling']['atm2wave'] == 0:
            print("""
            No coupling and data transfer from atmosphere to wave model
            """)
        elif modelSetup['Coupling']['atm2wave'] == 1:
            if nt == 0:
                os.system(('win_python ' + \
                    'dfs_forcing_first.py ' + TimeStamps[nt].strftime('%Y-%m-%d_%H_%M_%S') +' '+ \
                        str((TimeStamps[nt+1] - TimeStamps[nt]).total_seconds())))
            else:
                os.system(('win_python ' + \
                    'dfs_forcing_second.py ' + 'wrfout_d01_' + TimeStamps[nt+1].strftime('%Y-%m-%d_%H_%M_%S')))
#            print("Created Input File for MIKE")
            if os.path.isdir(os.path.join(modelSetup['UnixPath']['MIKE'], 'Result')) is False:
                os.mkdir(os.path.join(modelSetup['UnixPath']['MIKE'], 'Result'))
#
            print("\
            Running MIKE for timestep %r"%str(nt)+" \
            ")
            mike_preprocess_time = time.time()
            coupler_log.write('MIKE Preprocessing %d'%nt + ' %4.2f'%(mike_preprocess_time - wrf_end) + ' seconds' + '\n')
            coupler_log.flush()
#
            # time.sleep(3)
            # process = subprocess.Popen(["cmd","/C","run_mike.bat"], \
                # cwd = modelSetup['UnixPath']['MIKE'], stdout=sys.stdout, stderr=sys.stderr)
            process = subprocess.Popen(["FemEngineSWMPI.exe","pfs_input.sw"], \
                cwd = modelSetup['UnixPath']['MIKE'], stdout=sys.stdout, stderr=sys.stderr)
            process.communicate()
#
            mike_end = time.time()
            coupler_log.write('MIKE runtime %d'%nt + ' %4.2f'%(mike_end - mike_preprocess_time) + ' seconds' + '\n')
            coupler_log.flush()
#
            #remove hot file from last run
            if nt > 0:
                if os.path.isfile(os.path.join(modelSetup['UnixPath']['MIKE'], 'hotfile.dfsu')) is True:
                    os.remove(os.path.join(modelSetup['UnixPath']['MIKE'], 'hotfile.dfsu'))
#move MIKE archive to respective directory
            for ext in ['*.dfsu', '*.dfs0']:
                for filename in glob(os.path.join(modelSetup['UnixPath']['MIKE'], 'Result', ext)):
                    if filename == os.path.join(modelSetup['UnixPath']['MIKE'], 'Result', 'hotfile.dfsu'):
                        shutil.move(os.path.join(modelSetup['UnixPath']['MIKE'], 'Result', 'hotfile.dfsu'), \
                        os.path.join(modelSetup['UnixPath']['MIKE'], 'hotfile.dfsu'))
                    else:
                        if os.path.isdir(os.path.join(modelSetup['Archive']['MIKE'], \
                            'mike_out_'+TimeStamps[nt].strftime('%Y-%m-%d_%H_%M_%S'))) is False:
                            os.mkdir(os.path.join(modelSetup['Archive']['MIKE'], \
                            'mike_out_'+TimeStamps[nt].strftime('%Y-%m-%d_%H_%M_%S')))
##
                        move_file(source_path=os.path.join(modelSetup['UnixPath']['MIKE'], 'Result'), \
                            destination_path=os.path.join(modelSetup['Archive']['MIKE'], \
                            'mike_out_'+TimeStamps[nt].strftime('%Y-%m-%d_%H_%M_%S')), \
                            file_name=filename[len(os.path.join(modelSetup['UnixPath']['MIKE'], 'Result'))+1:])

            shutil.move(os.path.join(modelSetup['UnixPath']['MIKE'], 'pfs_input.log'), \
                os.path.join(modelSetup['LogPath']['MIKE'], \
                'mike_'+TimeStamps[nt].strftime('%Y-%m-%d_%H_%M_%S')+'.log'))
##
            os.system(('win_python mike2wrf.py %r'% modelSetup['WindowsPath']['WRF'] + ' %r'% \
                modelSetup['WindowsPath']['Archive'] +'%r'% '\\mike_out_' + \
                TimeStamps[nt].strftime('%Y-%m-%d_%H_%M_%S')))
        else:
            print("""
            Incorrect argument for atm2wave flag, value should be either 0 or 1
            """)
        """
        Feedback step from wave to atmosphere
        """
        if modelSetup['Coupling']['wave2atm'] == 0:
            print("""
            No feedback and data transfer from wave to atmosphere model
            """)
        elif modelSetup['Coupling']['wave2atm'] == 1:
            add_var2wrfrst.main(arg1=modelSetup['UnixPath']['WRF'], \
                arg2=modelSetup['Namelist']['WRF'], arg3='wrfrst_d01_' + TimeStamps[nt+1].strftime('%Y-%m-%d_%H_%M_%S'))
        else:
            print("""
            Incorrect argument for wave2atm flag, value should be either 0 or 1
            """)
        """
        Delete WRF restart file used in previous time step
        """
        if nt > 0:
            if os.path.isfile(os.path.join(modelSetup['UnixPath']['WRF'],'wrfrst_d01_'+TimeStamps[nt].strftime('%Y-%m-%d_%H_%M_%S'))):
                os.remove(os.path.join(modelSetup['UnixPath']['WRF'],'wrfrst_d01_'+TimeStamps[nt].strftime('%Y-%m-%d_%H_%M_%S')))

        end = time.time()
        runTime = (end - start)
        coupler_log.write('Timestep %d'%nt + ' %4.2f'%runTime + ' seconds' + '\n')
        coupler_log.write(' \n')
        coupler_log.flush()
elif run_coupler == 0:
    """
    here I will write the code which will allow the model to 
    run the uncoupled wave or atmosphere model
    """
    if int(modelSetup['Model']['atmosphere']) == 1:
        
        TimeStamps = timeManage.getTimestamps( \
        sT = datetime.strptime(modelSetup['Time']['start'], "%Y:%m:%d_%H:%M:%S"), \
        eT = datetime.strptime(modelSetup['Time']['end'], "%Y:%m:%d_%H:%M:%S"), \
        min = datetime.strptime(modelSetup['Time']['step'], "%H:%M:%S").minute, \
        hrs = datetime.strptime(modelSetup['Time']['step'], "%H:%M:%S").hour, \
        sec = datetime.strptime(modelSetup['Time']['step'], "%H:%M:%S").second)
        if os.path.isdir(modelSetup['Archive']['WRF']) == False:
            os.path.mkdir(modelSetup['Archive']['WRF'])
        for nt in xrange(len(TimeStamps) - 1):
            start = time.time()
            if nt == 0:
                add_var_met.main(modelSetup['InputData']['metgrid'])
                metgrid_file_list = glob(os.path.join(modelSetup['InputData']['metgrid'], \
                'met_em*'))
                for metgrid_file in metgrid_file_list:
                    make_symbolic_link(source_path = modelSetup['InputData']['metgrid'], \
                        destination_path = modelSetup['UnixPath']['WRF'], \
                        file_name = metgrid_file)
                timeManage.main(modelConfig = modelSetup, TimeStamps = TimeStamps, Nstep = nt)

                modelRunner.run_model(model_name = 'wrf.exe', \
                    path_to_dir = modelSetup['UnixPath']['WRF'])
                #making log files
                if os.path.isfile(os.path.join(os.getcwd(), 'model_out.log')):
                    shutil.move(os.path.join(os.getcwd(), 'model_out.log'), \
                    os.path.join(modelSetup['LogPath']['WRF'], \
                    'wrfout_'+TimeStamps[nt].strftime("%Y-%m-%d_%H_%M_%S")+'.log'))
                if os.path.isfile(os.path.join(os.getcwd(), 'model_error.log')):
                    shutil.move(os.path.join(os.getcwd(), 'model_error.log'), \
                    os.path.join(modelSetup['LogPath']['WRF'], \
                    'wrferror_'+TimeStamps[nt].strftime("%Y-%m-%d_%H_%M_%S")+'.log'))
                    
                move_file(source_path = modelSetup['UnixPath']['WRF'], \
                destination_path = modelSetup['Archive']['WRF'], \
                file_name = "wrfout_d01_"+TimeStamps[nt].strftime("%Y-%m-%d_%H_%M_%S"))
            else:
                timeManage.main(modelConfig = modelSetup, TimeStamps = TimeStamps, Nstep = nt)
                modelRunner.run_model(model_name = 'wrf.exe', \
                    path_to_dir = modelSetup['UnixPath']['WRF'])

                os.remove(os.path.join(modelSetup['UnixPath']['WRF'], \
                "wrfrst_d01_"+TimeStamps[nt].strftime("%Y-%m-%d_%H_%M_%S")))

                move_file(source_path = modelSetup['UnixPath']['WRF'], \
                destination_path = modelSetup['Archive']['WRF'], \
                file_name = "wrfout_d01_"+TimeStamps[nt].strftime("%Y-%m-%d_%H_%M_%S"))
                #making log files
                if os.path.isfile(os.path.join(os.getcwd(), 'model_out.log')):
                    shutil.move(os.path.join(os.getcwd(), 'model_out.log'), \
                    os.path.join(modelSetup['LogPath']['WRF'], \
                    'wrfout_'+TimeStamps[nt].strftime("%Y-%m-%d_%H_%M_%S")+'.log'))
                if os.path.isfile(os.path.join(os.getcwd(), 'model_error.log')):
                    shutil.move(os.path.join(os.getcwd(), 'model_error.log'), \
                    os.path.join(modelSetup['LogPath']['WRF'], \
                    'wrferror_'+TimeStamps[nt].strftime("%Y-%m-%d_%H_%M_%S")+'.log'))

            end = time.time()
            runTime = (end - start)
            coupler_log.write('Timestep %d'%nt + ' %4.2f'%runTime + ' seconds' + '\n')
            coupler_log.write(' \n')
            coupler_log.flush()
    elif int(modelSetup['Model']['wave']) == 1:
        pass
    elif int(modelSetup['Model']['ocean']) == 1:
        pass
else:
    print("""
    No model is configured to run, bye
    """)

# move_file(source_path = modelSetup['UnixPath']['WRF'], destination_path = modelSetup['Archive']['WRF'], \
# file_name = "wrfout_d01_"+TimeStamps[nt+1].strftime("%Y-%m-%d_%H_%M_%S"))

end = """
    Finished Running the model, 
    Bye
"""
print(end)

if configFile is True:
    configFile.close()
coupler_log.close()