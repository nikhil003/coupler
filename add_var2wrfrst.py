from function_libraries import Dataset, h5py
from function_libraries import os, sys
"""

written by : Nikhil
purpose: add MIKE input to WRF RST file

"""

def main(arg1 = None, arg2 = None, arg3 = None):
    if (arg1 is None) or (arg2 is None) or (arg3 is None):
        print("""
        Arguments arg1, arg2 and arg3 must not be empty. Check and try again
        """)
    else:
        wrf_dir = arg1
        home_dir = arg2

        mikeFile = h5py.File(os.path.join(home_dir, 'mike_data.h5'),'r')
        rst_file = Dataset(os.path.join(wrf_dir, arg3) , 'a')

        rst_file.variables['LWAVEP'][0,:,:] = mikeFile['lwave'][:, :]
        rst_file.variables['TWAVEP'][0,:,:] = mikeFile['twave'][:, :]
        rst_file.variables['HWAVEP'][0,:,:] = mikeFile['hwave'][:, :]
        rst_file.variables['Z0WAVE'][0,:,:] = mikeFile['z0wave'][:, :]

        rst_file.close()
        mikeFile.close()

if __name__ == "__main__":

    nargs=len(sys.argv)

    if nargs < 3:
        print(" <WRF DIRECTORY>     \n\
                <Home DIRECTORY>    \n\
                <WRF RST FILE NAME> \n\
            ")
        sys.exit(-1)

    main(arg1 = sys.argv[1], arg2 = sys.argv[2], arg3 = sys.argv[3])