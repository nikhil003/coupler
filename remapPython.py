#-----------------------------------------------
#python + python .net packages
#-----------------------------------------------
import os
import clr
from clr import System
from System import Reflection, Array, Single
from glob import glob
import numpy as np
#----------------------------------------------
#dll files from DHI toolbox
#----------------------------------------------
#get list of dll in directory
#----------------------------------------------
dll_list = glob(os.path.join(os.getcwd(), '*.dll'))
dll_list.sort()
#----------------------------------------------
#add complete path for python net to find it
#----------------------------------------------
for dllName in dll_list:
    Reflection.Assembly.LoadFile(dllName)
#----------------------------------------------
#import DHI module
#----------------------------------------------
import DHI
from DHI.Generic.MikeZero import DFS
from DHI.Generic.MikeZero.DFS.dfs123 import *
from DHI.Generic.MikeZero.DFS import *
#----------------------------------------------------------------------
def calculate_node_vals(num, n2e, x_e, y_e, z_e, x_n, y_n, varname=None):
    Rx = 0.; Ry = 0.;
    Ixx = 0.; Iyy = 0.; Ixy = 0.
    omega = 0.; ztemp = 0.
    #-------------------------------------------------
    #pseudo laplace
    #-------------------------------------------------
    nelem = len(np.where(n2e[num, :]> 0)[0])
    for i in xrange(nelem):
        id = n2e[num, i] - 1
        #print num, id
        dx = x_e[id] - x_n[num]
        dy = y_e[id] - y_n[num]
        Rx += dx
        Ry += dy
        Ixx += dx ** 2
        Iyy += dy ** 2
        Ixy += dx * dy
    lamda = Ixx * Iyy - Ixy * Ixy 
    if np.abs(lamda) > 1e-10 * (Ixx * Iyy):
        lamda_x  = (Ixy * Ry - Iyy * Rx) / lamda
        lamda_y  = (Ixy * Rx - Ixx * Ry) / lamda
        omega_sum = 0.; ztemp = 0.
        for i in xrange(nelem):
            id = n2e[num, i] - 1
            omega = 1.0 + lamda_x * (x_e[id] - x_n[num]) + lamda_y * (y_e[id] - y_n[num])
            if omega < 0:
                omega = 0.
            elif omega > 2:
                omega = 2.
            omega_sum += omega
            ztemp = ztemp + z_e[id] * omega 
        
        if np.abs(omega_sum) > 1e-10:
            ztemp /=  omega_sum
        else:
            omega_sum = 0.
    else:
        omega_sum = 0.
    #---------------------------------------------
    #inverse distance weighted
    #---------------------------------------------
    if omega_sum == 0:
        # if varname != None:
            # print varname, num
        for i in xrange(nelem):
            id = n2e[num, i] - 1
            dx = x_e[id] - x_n[num]
            dy = y_e[id] - y_n[num]
            omega = 1.0 / np.sqrt(dx * dx  + dy * dy)
            omega_sum = omega_sum + omega
            ztemp = ztemp + omega * z_e[id]
        if omega_sum != 0.:
            #print ztemp, omega_sum
            ztemp = ztemp / omega_sum
        else:
            ztemp = 0.0
    return ztemp
#----------------------------------------------------------------------
def lon_lat_to_cartesian(lon, lat, R = 1):
    """
    calculates lon, lat coordinates of a point on a sphere with
    radius R
    """
    lon_r = np.radians(lon)
    lat_r = np.radians(lat)

    x =  R * np.cos(lat_r) * np.cos(lon_r)
    y = R * np.cos(lat_r) * np.sin(lon_r)
    z = R * np.sin(lat_r)
    return x,y,z
#----------------------------------------------------------------------
def get_vars(in_file, nsteps, varlist = None, dump_vars = None):
    """
    Extract the values from the grid point file
    and save it to a dictionary
    """
    if varlist != None:
        nvars = len(varlist)
    else:
        nvars = 1
    nsteps = nsteps
    var_dict = {}
    if varlist is None:
        for n in xrange(nvars):
            temp_var = np.zeros(shape = nsteps, dtype = object)
            for i in xrange(nsteps):
                #print n
                temp_var[i] = in_file.ReadItemTimeStep(n+1, i).Data
            var_dict[n] = temp_var
    else:
        for n in xrange(nvars):
            temp_var = np.zeros(shape = nsteps, dtype = object)
            for i in xrange(nsteps):
                #print n
                temp_var[i] = in_file.ReadItemTimeStep(varlist[n], i).Data
            var_dict[n] = temp_var      
    var_values = {}
#-------------------------------------------------------------
    for n in xrange(nvars):
        npoints = len(var_dict[n][0])
        #print npoints
        temp = np.zeros(shape = (nsteps, npoints))
        for i in xrange(nsteps):
            for j in xrange(npoints):
                temp[i, j] = var_dict[n][i][j]
        var_values[n] = temp
#-------------------------------------------------------------
    if dump_vars != None:
        var_names = ['swh','peak_wp','wp_t02','mean_wd','ux','uy']
        if nvars >= 1:
            for n in xrange(nvars):
                dump_var(var_values[n], True, var_names[n])
                print var_values[n][10, 0]
        else:
            dump_var(var_values, True)
    return  var_values
#----------------------------------------------------------------------
def get_grid(in_file):
    """
    To extract grid nodes from the MIKE file
    """
    gridx = np.fromiter(in_file.X, np.float)
    gridy = np.fromiter(in_file.Y, np.float)
    gridz = np.fromiter(in_file.Z, np.float)
    return gridx, gridy, gridz
#---------------------------------------------------------------------- 
def make_tritable(in_file):
    """
    To create a tri table from MIKE dfsu file
    """
    num_ele = len(in_file.ElementTable)
    tri_tables = []
    
    for i in xrange(num_ele):
        each_node = []
        temp = len(in_file.ElementTable[i])
        for j in xrange(temp):
            each_node.append(in_file.ElementTable[i][j])
        tri_tables.append(each_node)

    tri_tables = np.array(tri_tables)

    tri_tables = np.subtract(tri_tables, 1) 
    
    return tri_tables
#---------------------------------------------------------------------- 
def NtoE(xn, yn, zn, tritable):
    """
    Convert node coordinates to element centres
    >>> xe, ye, ze = NtoE(xn, yn, zn, tritable)
    tritable is numpy array
    """
    num_ele = tritable.shape[0]
    xe = np.zeros(shape = (num_ele))
    ye = np.zeros_like(xe) 
    ze = np.zeros_like(xe)

    for i in xrange(num_ele):
        xe[i] = np.average([xn[tritable[i, 0]],  xn[tritable[i, 1]], xn[tritable[i, 2]]], axis=0)
        ye[i] = np.average([yn[tritable[i, 0]],  yn[tritable[i, 1]], yn[tritable[i, 2]]], axis=0)
        ze[i] = np.average([zn[tritable[i, 0]],  zn[tritable[i, 1]], zn[tritable[i, 2]]], axis=0)   
        
    return xe,ye,ze
#----------------------------------------------------------------------     
def get_N2E(tritable):
    """
    Created Node to Element
    >>> n2e = get_N2E(tritables)
    """
    num_ele = tritable.shape[0]
    e = np.linspace(1, num_ele, num_ele)
    I = np.array([e, e, e]).flatten()
    J = tritable.flatten('F'); 

    temp = np.zeros(shape = (np.max(tritable)+1 , 10)); 
    count = np.zeros(shape = (np.max(tritable)+1))

    for i in xrange(len(I)):
        count[J[i]] += 1
        temp[J[i], count[J[i]]-1] = I[i]    
    return temp
    
def extractDFSU(filein, dfsuvars, timesteps):
    """
    This function will extract the data from dfsu file, stores it in an 
    array and then in a dictionary
    """
    data = {}
    for var in dfsuvars:
        data[var] = np.zeros(shape = (timesteps, len(filein.ReadItemTimeStep(1, 0).Data)), \
        dtype=float)
    for j in xrange(nsteps):
        for i in xrange(len(dfsuvars)):         
            data[dfsuvars[i]][j,:] = np.fromiter(filein.ReadItemTimeStep(i+1, j).Data, \
            dtype=float)
    return data
        
def extractDFS0(filein, buoys, dfs0vars, timesteps):
    """
    This function is used to extract the data from dfs0 file 
    and read it in bulk and then sort it in correct order so 
    to make numpy array and dictionary
    """
    data = Dfs0Util.ReadDfs0DataDouble(filein)
    nx = data.GetLength(0)
    ny = data.GetLength(1)
    if nx != timesteps:
        if nx > timesteps:
            delT = nx - timesteps
        else:
            print "Not sure about timesteps in dfs0 file, check for accuracy"
            sys.exit(-1)
            
    if ny != len(buoys) * len(dfs0vars):
        if ny > len(buoys) * len(dfs0vars):
            delV = ny - len(buoys) * len(dfs0vars)
        else:
            print "Not sure about number of data items in dfs0 file, check for accuracy"
            print "either number of variables or number of buoys is not right"
            sys.exit(-1)            
            
    dataout = {}
    for var in dfs0vars:
        dataout[var] = np.zeros(shape = (len(buoys), timesteps), dtype=float)
    for n in xrange(len(buoys)):
        for i in xrange(len(dfs0vars)):
            for k in xrange(timesteps):
                dataout[dfs0vars[i]][n, k] = data[k+delT, n + i * len(buoys) + delV]    
    return dataout

def extractVarName(filein):
    """
    It extract varnames from the dfsu file
    and return in form of a list
    """
    nvars = filein.ItemInfo.Count
    vars = []
    for i in xrange(nvars):
        vars.append(filein.ItemInfo[i].Name)
    return vars
    
def extractStations(filein):
    """
    Extract the names of the vars and buoy locations 
    from dfs0 file in correct order. It will help in sorting 
    and creating hdf5 file
    """
    count = filein.ItemInfo.Count
    stations = []
    varnames = []
    for i in xrange(count):
        if filein.ItemInfo[i].Name[3] == ':':
            if filein.ItemInfo[i].Name[4:] in varnames:
                pass
            else:
                varnames.append(filein.ItemInfo[i].Name[4:])
            if filein.ItemInfo[i].Name[:3] in stations:
                pass
            else:
                stations.append(filein.ItemInfo[i].Name[:3])
        elif filein.ItemInfo[i].Name[4] == ':':
            if filein.ItemInfo[i].Name[5:] in varnames:
                pass
            else:
                varnames.append(filein.ItemInfo[i].Name[5:])            
            if filein.ItemInfo[i].Name[:4] in stations:
                pass
            else:   
                stations.append(filein.ItemInfo[i].Name[:4])    
    return stations, varnames   