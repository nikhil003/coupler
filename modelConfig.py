from function_libraries import json, os, sys, io

def makeJson(inFile, outName = None):
    """
        In order to make a json file using python json library
        input = model_config file
        output = model_config.json
        Example:
        >>> from modelConfig import makeJson
        >>> makeJson("./model_config")
    """
    with open(inFile) as fileIn:
        for row in fileIn.readlines():
            if row.startswith("year_a"):
                start = str(row.split()[-1])
            elif row.startswith("year_b"):
                end = str(row.split()[-1])
            elif row.startswith("tstep"):
                step = str(row.split()[-1])
            elif row.startswith("atm2oc"):
                atm2oc = int(row.split()[-1])
            elif row.startswith("oc2atm"):
                oc2atm = int(row.split()[-1])
            elif row.startswith("atm2wave"):
                atm2wave = int(row.split()[-1])
            elif row.startswith("wave2atm"):
                wave2atm = int(row.split()[-1])
            elif row.startswith("atmosphere"):
                run_atmosphere_model = int(row.split()[-1])
            elif row.startswith("wave"):
                run_wave_model = int(row.split()[-1])
            elif row.startswith("ocean"):
                run_ocean_model = int(row.split()[-1])
            elif row.startswith("wrf_dir"):
                wrf_upath = str(row.split()[-1])
            elif row.startswith("mike_dir"):
                mike_upath = str(row.split()[-1])
            elif row.startswith("namelist_loc"):
                wrf_nm_path = str(row.split()[-1])
            elif row.startswith("mike_pfs_loc"):
                mike_pfs_path = str(row.split()[-1])
            elif row.startswith("wrf_win_path"):
                wrf_winpath = str(row.split()[-1])
            elif row.startswith("mike_win_path"):
                mike_winpath = str(row.split()[-1])
            elif row.startswith("mike_win_archive"):
                mike_win_archive = str(row.split()[-1])
            elif row.startswith("mike_archive"):
                mike_archive_path = str(row.split()[-1])
            elif row.startswith("wrf_archive"):
                wrf_archive_path = str(row.split()[-1])
            elif row.startswith("metgrid_file_path"):
                metgrid_path = str(row.split()[-1])
            elif row.startswith("model_run_type"):
                run_type = str(row.split()[-1])
            elif row.startswith("delta_x"):
                grid_step_size = float(row.split()[-1])
            elif row.startswith("wrf_log"):
                wrf_log = str(row.split()[-1])
            elif row.startswith("mike_log"):
                mike_log = str(row.split()[-1])
            elif row.startswith("remapping_lib"):
                remap = str(row.split()[-1])
            elif row.startswith("extractVariables"):
                varnum = row.split()[2:]
                for i in xrange(len(varnum)):
                    if i < (len(varnum)-1):
                        varnum[i] = int(varnum[i][:-1])
                    else:
                        varnum[i] = int(varnum[i])
    fileout = {
        "Time" :{
        "start" :   start, 
        "end" : end,
        "step" :    step
        },
        "Coupling":{
        "atm2wave": atm2wave,
        "wave2atm": wave2atm,
        "atm2oc": atm2oc,
        "oc2atm": oc2atm
        },
        "UnixPath":{
        "WRF": wrf_upath,
        "MIKE": mike_upath
        },
        "WindowsPath":{
        "WRF": wrf_winpath,
        "MIKE": mike_winpath,
        "Archive": mike_win_archive
        },
        "Namelist":{
        "WRF": wrf_nm_path,
        "MIKE": mike_pfs_path
        },
        "Archive":{
        "WRF": wrf_archive_path,
        "MIKE": mike_archive_path
        },
        "Model":{
        "atmosphere": run_atmosphere_model,
        "wave": run_wave_model,
        "ocean": run_ocean_model
        },
        "InputData":{
        "metgrid": metgrid_path
        },
        "RunType":{
        "WRF": run_type
        },
        "Grid":{
        "StepSize": grid_step_size
        },
        "LogPath":{
        "WRF": wrf_log,
        "MIKE": mike_log
        },
        "remapping_lib": remap,
        "Vars":{
        "Varnum":varnum
        }
    }
    #print fileout
    if outName is None:
        outFile = "model_config.json"
    else:
        outFile = outName
    with open(outFile, "w") as filename:
        json.dump(fileout, filename, indent=4, sort_keys=True, separators=(',',':'))
    fileIn.close()

if __name__ == "__main__":
    if os.path.exists(os.path.join(os.getcwd(), "model_config")) == True:
        makeJson(os.path.join(os.getcwd(), "model_config"))
    else:
        print("File model_config does not exist")
        print("Check path for model_config file")
        