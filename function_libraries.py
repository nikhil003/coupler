"""
Function for importing Libraries from the python installation, 
No test and library checks have been included and would be better 
if these things are added and would make me more sophisticated
"""
#default library from python
import json
import os, sys, io
from datetime import datetime, timedelta
from glob import glob
from math import floor
import time
import shutil
import subprocess
#installed library 
try:
    from numpy import shape, empty, power, sqrt, maximum, pi, zeros, ravel, fromiter, \
        multiply, add, divide, reshape, array
except ImportError:
    print("No Numpy module in the python installation")

try:
    import h5py
    import hickle as hkl
except ImportError:
    print("No HDF5 module in python installation")

try:
    from netCDF4 import Dataset, MFDataset
except ImportError:
    print("No NETCDF4 module in python installation")
