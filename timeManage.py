from mikePFS import MIKEnamelist
from wrfNamelist import WRFnamelist
from function_libraries import os, sys, json, array, time, shutil
from function_libraries import datetime, timedelta
import modelRunner

def getTimestamps(sT = None, eT = None, min = None, hrs = None, sec = None):
    """
    Make Timestamps by taking input from the 
    model config file
    """
    timeStamps = []
    nsteps = int((eT - sT).total_seconds()) \
     / int(hrs * 3600 + min * 60)
    for i in xrange(nsteps+1):
        timeStamps.append(sT + timedelta(seconds = i * int(hrs * 3600 + min * 60 + sec)))
    timeStamps = array(timeStamps, dtype=object)
    return timeStamps

def main(modelConfig = None, TimeStamps = None, Nstep = None):
    """
    Purpose:
    This function is used to create and copy the namelist and pfs file, for 
    running mike and WRF model. 
    """
    #start_Time = time.time()
    if modelConfig is None:
        if os.path.isfile(os.path.join(os.getcwd(), 'model_config.json')):
            with open(os.path.join(os.getcwd(), 'model_config.json')) as fileIn:
                configFile = json.load(fileIn)
        else:
            print "Cannot locate the model_config file"
            print "Enter correct directory"
            sys.exit(-1)
    else:
        configFile = modelConfig

    TimeStamps = getTimestamps( \
    sT = datetime.strptime(configFile['Time']['start'], "%Y:%m:%d_%H:%M:%S"), \
    eT = datetime.strptime(configFile['Time']['end'], "%Y:%m:%d_%H:%M:%S"), \
    min = datetime.strptime(configFile['Time']['step'], "%H:%M:%S").minute, \
    hrs = datetime.strptime(configFile['Time']['step'], "%H:%M:%S").hour, \
    sec = datetime.strptime(configFile['Time']['step'], "%H:%M:%S").second)

    if (configFile['Coupling']['wave2atm'] == 1) and \
            (int(configFile['Model']['atmosphere']) == 1) and \
            (int(configFile['Model']['wave']) == 1):
        make_namelist = 1
    else:
        make_namelist = 0
    """
    Checking the flags for the model configuration, I have tried to be more 
    explicit while at the same time making sure that these scripts can 
    run both coupled and uncoupled version of model.
    """
    if configFile['Coupling']['atm2wave'] == 1 and \
            (int(configFile['Model']['atmosphere']) == 1) and \
            (int(configFile['Model']['wave']) == 1):
        make_pfs = 1
    else:
        make_pfs = 0
##
    if make_pfs == 1:
        """
        This step is for making the mike pfs file, which is needed by MIKE model 
        to run. In this step a new file is created as well as copied to MIKE 
        directory as given in the model_config.json file
        """
        #print("Making MIKE SW PFS file")
        if os.path.isfile(os.path.join(configFile['Namelist']['MIKE'], 'pfs_input.sw')):
            mikeIn = open(os.path.join(configFile['Namelist']['MIKE'], \
                'pfs_input.sw'), 'r')
            mikeOut = open(os.path.join(configFile['Namelist']['MIKE'], \
                'new_pfs_input.sw'), 'w')
        else:
            print("""Could not locate the mike pfs file at the specified location, 
            check the file name or the path
            """)
        if (TimeStamps is None) or (Nstep is None):
            print("""
            TimeStamps or Nstep cannot be None, Check and Enter Correct argument
            """)
            if Nstep > len(TimeStamps):
                print("""
                Time step instant cannot be greater than total Number of time steps
                """)
        else:
            MIKEnamelist(inputFile=mikeIn, outputFile=mikeOut, timeStamps=TimeStamps, TimeStep=Nstep)
            mikeIn.close()
            mikeOut.close()
            shutil.copyfile(os.path.join(configFile['Namelist']['MIKE'], \
                    'new_pfs_input.sw'), os.path.join(configFile['UnixPath']['MIKE'], \
                    'pfs_input.sw'))
    else:
        """
        This step is only used when the coupling is turned off, and MIKE model is 
        being run in uncoupled configuration
        """
        if configFile['Model']['wave'] == 1:
            shutil.copyfile(os.path.join(configFile['Namelist']['MIKE'], \
                    'pfs_input.sw'), os.path.join(configFile['UnixPath']['MIKE'], \
                    'pfs_input.sw'))
##
    if make_namelist == 1:
        """
        This steps is used to make the namelist file for the WRF model. Here the 
        namelist file are created and then copied over to the WRF directory as given 
        in the model_config.json
        """
        #print("Making WRF namelist file")
        if os.path.isfile(os.path.join(modelConfig['Namelist']['WRF'], 'namelist.input')):
            #for i in xrange(2):
            if os.path.isfile(os.path.join(modelConfig['Namelist']['WRF'], \
                'new_namelist.input')):
                os.remove(os.path.join(modelConfig['Namelist']['WRF'], \
                'new_namelist.input'))
            wrfIn = open(os.path.join(modelConfig['Namelist']['WRF'], \
                'namelist.input'), 'r')
            wrfOut = open(os.path.join(modelConfig['Namelist']['WRF'], \
                'new_namelist.input'), 'w')
        else:
            print("""Could not locate the wrf namelist file at the specified location, 
            check the file name or the path
            """)
##
        if (TimeStamps is None) or (Nstep is None):
            print("""
            TimeStamps or Nstep cannot be None, Check and Enter Correct argument
            """)
        elif Nstep > len(TimeStamps):
            print("""
            Time step instant cannot be greater than total Number of time steps
            """)
        else:
            WRFnamelist(inputfile=wrfIn, outputfile=wrfOut, timestamps=TimeStamps, \
                TimeStep=Nstep, real_run = True)
            wrfIn.close()
            wrfOut.close()
            if os.path.islink(os.path.join(configFile['UnixPath']['WRF'], \
                    'namelist.input')):
                    os.unlink(os.path.join(configFile['UnixPath']['WRF'], \
                    'namelist.input'))
            elif os.path.isfile(os.path.join(configFile['UnixPath']['WRF'], \
                    'namelist.input')):
                    os.remove(os.path.join(configFile['UnixPath']['WRF'], \
                    'namelist.input'))
            shutil.copyfile(os.path.join(configFile['Namelist']['WRF'], \
                    'new_namelist.input'), os.path.join(configFile['UnixPath']['WRF'], \
                    'namelist.input'))
            if Nstep == 0:
                if modelConfig['RunType']['WRF'] == 'real':
                    print("""
                    Running Real
                    """)
                    modelRunner.run_model(model_name = 'real.exe', \
                        path_to_dir = configFile['UnixPath']['WRF'])
##
                    if os.path.isfile(os.path.join(os.getcwd(), 'model_out.log')):
                        shutil.move(os.path.join(os.getcwd(), 'model_out.log'), \
                        os.path.join(modelConfig['LogPath']['WRF'], 'real_output.log'))
                    if os.path.isfile(os.path.join(os.getcwd(), 'model_error.log')):
                        shutil.move(os.path.join(os.getcwd(), 'model_error.log'), \
                        os.path.join(modelConfig['LogPath']['WRF'], 'real_error.log'))
##
                elif modelConfig['RunType']['WRF'] == 'ideal':
                    print("Running Ideal")
                    modelRunner.run_model(model_name = 'ideal.exe', \
                        path_to_dir = configFile['UnixPath']['WRF'])
##
                    if os.path.isfile(os.path.join(os.getcwd(), 'model_out.log')):
                        shutil.move(os.path.join(os.getcwd(), 'model_out.log'), \
                        os.path.join(modelConfig['LogPath']['WRF'], 'ideal_output.log'))
                    if os.path.isfile(os.path.join(os.getcwd(), 'model_error.log')):
                        shutil.move(os.path.join(os.getcwd(), 'model_error.log'), \
                        os.path.join(modelConfig['LogPath']['WRF'], 'ideal_error.log'))
##
        wrfIn.close()
        wrfIn = open(os.path.join(modelConfig['Namelist']['WRF'], \
            'namelist.input'), 'r')
        if os.path.isfile(os.path.join(modelConfig['Namelist']['WRF'], \
            'new_namelist.input')):
            os.remove(os.path.join(modelConfig['Namelist']['WRF'], \
            'new_namelist.input'))
        wrfOut = open(os.path.join(modelConfig['Namelist']['WRF'], \
            'new_namelist.input'), 'w')
##
        WRFnamelist(inputfile=wrfIn, outputfile=wrfOut, timestamps=TimeStamps, \
            TimeStep=Nstep)
##
        wrfOut.close()
        wrfIn.close()
        if os.path.islink(os.path.join(configFile['UnixPath']['WRF'], \
                'namelist.input')):
                os.unlink(os.path.join(configFile['UnixPath']['WRF'], \
                'namelist.input'))
        elif os.path.isfile(os.path.join(configFile['UnixPath']['WRF'], \
                'namelist.input')):
                os.remove(os.path.join(configFile['UnixPath']['WRF'], \
                'namelist.input'))
        shutil.copyfile(os.path.join(configFile['Namelist']['WRF'], \
                'new_namelist.input'), os.path.join(configFile['UnixPath']['WRF'], \
                'namelist.input'))
    else:
        """
        Same as above, this step is only used when the coupling step is turned 
        off and only WRF model is being used in uncoupled configuration
        """
        # if configFile['Model']['atmosphere'] == 1:
            # shutil.copyfile(os.path.join(configFile['Namelist']['WRF'], \
                    # 'namelist.input'), os.path.join(configFile['UnixPath']['WRF'], \
                    # 'namelist.input'))
        """
        this bit is for dummy run to test whether the code is working as it is supposed to 
        work
        """
        wrfIn = open(os.path.join(modelConfig['Namelist']['WRF'], \
            'namelist.input'), 'r')
        if Nstep == 0:
            if os.path.isfile(os.path.join(modelConfig['Namelist']['WRF'], \
                'new_namelist.input')):
                os.remove(os.path.join(modelConfig['Namelist']['WRF'], \
                'new_namelist.input'))
            wrfOut = open(os.path.join(modelConfig['Namelist']['WRF'], \
                'new_namelist.input'), 'w')

            WRFnamelist(inputfile=wrfIn, outputfile=wrfOut, timestamps=TimeStamps, \
                TimeStep=Nstep, real_run = True)

            wrfOut.close()
            """
            after running real, next step will be to run wrf for first time step
            for this step, need to make namelist file again
            """
            if os.path.islink(os.path.join(configFile['UnixPath']['WRF'], \
                    'namelist.input')):
                    os.unlink(os.path.join(configFile['UnixPath']['WRF'], \
                    'namelist.input'))
            elif os.path.isfile(os.path.join(configFile['UnixPath']['WRF'], \
                    'namelist.input')):
                    os.remove(os.path.join(configFile['UnixPath']['WRF'], \
                    'namelist.input'))
            shutil.copyfile(os.path.join(configFile['Namelist']['WRF'], \
                    'new_namelist.input'), os.path.join(configFile['UnixPath']['WRF'], \
                    'namelist.input'))
            # print("Made Namelist for Real, Will run Real now")
            if modelConfig['RunType']['WRF'] == 'real':
                modelRunner.run_model(model_name = 'real.exe', \
                    path_to_dir = configFile['UnixPath']['WRF'])
                #creating log files and copying them to desired directory
                if os.path.isfile(os.path.join(os.getcwd(), 'model_out.log')):
                    shutil.move(os.path.join(os.getcwd(), 'model_out.log'), \
                    os.path.join(modelConfig['LogPath']['WRF'], 'real_output.log'))
                if os.path.isfile(os.path.join(os.getcwd(), 'model_error.log')):
                    shutil.move(os.path.join(os.getcwd(), 'model_error.log'), \
                    os.path.join(modelConfig['LogPath']['WRF'], 'real_error.log'))
            elif modelConfig['RunType']['WRF'] == 'ideal':
                modelRunner.run_model(model_name = 'ideal.exe', \
                    path_to_dir = configFile['UnixPath']['WRF'])
                if os.path.isfile(os.path.join(os.getcwd(), 'model_out.log')):
                    shutil.move(os.path.join(os.getcwd(), 'model_out.log'), \
                    os.path.join(modelConfig['LogPath']['WRF'], 'ideal_output.log'))
                if os.path.isfile(os.path.join(os.getcwd(), 'model_error.log')):
                    shutil.move(os.path.join(os.getcwd(), 'model_error.log'), \
                    os.path.join(modelConfig['LogPath']['WRF'], 'ideal_error.log'))
        wrfIn.close()
        wrfIn = open(os.path.join(modelConfig['Namelist']['WRF'], \
            'namelist.input'), 'r')
        if os.path.isfile(os.path.join(modelConfig['Namelist']['WRF'], \
            'new_namelist.input')):
            os.remove(os.path.join(modelConfig['Namelist']['WRF'], \
            'new_namelist.input'))
        wrfOut = open(os.path.join(modelConfig['Namelist']['WRF'], \
            'new_namelist.input'), 'w')
##
        WRFnamelist(inputfile=wrfIn, outputfile=wrfOut, timestamps=TimeStamps, \
            TimeStep=Nstep, real_run = False)
##
        wrfOut.close()
        wrfIn.close()
        if os.path.islink(os.path.join(configFile['UnixPath']['WRF'], \
                'namelist.input')):
                os.unlink(os.path.join(configFile['UnixPath']['WRF'], \
                'namelist.input'))
        elif os.path.isfile(os.path.join(configFile['UnixPath']['WRF'], \
                'namelist.input')):
                os.remove(os.path.join(configFile['UnixPath']['WRF'], \
                'namelist.input'))
        shutil.copyfile(os.path.join(configFile['Namelist']['WRF'], \
                'new_namelist.input'), os.path.join(configFile['UnixPath']['WRF'], \
                'namelist.input'))
    #end_time = time.time()
    #print ("Time taken in running = %2.5f sec" % (end_time - start_Time))

if __name__ == "__main__":
    main()
