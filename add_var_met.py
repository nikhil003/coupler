from function_libraries import shape, empty, power, sqrt, maximum, pi, zeros, add
from function_libraries import Dataset, os, glob
"""

written by : Nikhil
purpose:
Add variables (LWAVEP, TWAVEP, Z0WAVE)
to WRF metgrid file using wind speeds 
from metgrid file

"""

def vel_masspoints(inarray, comment=None):
    if comment is 'u':
        xdim = shape(inarray)[-1] - 1
        ydim = shape(inarray)[-2]        
        temp = empty(shape = (ydim, xdim))
        for i in xrange(xdim - 1):
            temp[:, i] = 0.5 *(inarray[:,i] + inarray[:,i+1])
    elif comment is 'v':
        xdim = shape(inarray)[-1] 
        ydim = shape(inarray)[-2] - 1        
        temp = empty(shape = (ydim, xdim))
        for i in xrange(ydim - 1):
            temp[i, :] = 0.5 *(inarray[i,:] + inarray[i+1, :])
    return temp

def rotate_winds(var1, var2, cos_var, sin_var):
        temp_u = var1 * cos_var + var2 * sin_var
        temp_v = var1 * cos_var - var2 * sin_var
        return temp_u, temp_v

def magnitude(var1, var2):
        temp = sqrt(add(power(var1, 2), power(var2, 2)))
        return temp

def waveperiod(var):
        temp = 0.729 * maximum(var, 0.1)
        return temp

def wavelength(var):
        g = 9.81
        temp = g * waveperiod(var) ** 2 / (2 * pi)
        return temp
        
def waveheight(var):
        temp = 0.0248 * var ** 2
        return temp

def Wu_ust(u10):
    """
    Calculate Ust from U10 using Wu(1980) relation
    """
    cd = 0.001 * (0.08 + 0.065 * u10)
    ust = sqrt(cd) * u10
    return ust

def main(metgrid_file_path=None):
    
    if metgrid_file_path is None:
        file_list = glob(os.path.join(os.getcwd(), 'met_em*'))
    else:
        file_list = glob(os.path.join(metgrid_file_path, 'met_em*'))

    if len(file_list) < 1:
        print("""Path of Metgrid file is not correct
               Check the path and try again""")
    else:
        num_file = len(file_list)

        for i in xrange(num_file):
            file_in = Dataset(file_list[i])
            lons = file_in.variables['XLONG_M'][0,:,:]
            lats = file_in.variables['XLAT_M'][0,:,:]
            u_in = file_in.variables['UU'][0,0,:,:]
            v_in = file_in.variables['VV'][0,0,:,:]
            cosalpha = file_in.variables['COSALPHA'][0,:,:]
            sinalpha = file_in.variables['SINALPHA'][0,:,:] 

            uvel, vvel = vel_masspoints(u_in, 'u'), vel_masspoints(v_in, 'v')           
            wind_velocity = magnitude(uvel, vvel)
            xdim = wind_velocity.shape[1]; ydim = wind_velocity.shape[0]
            twave = zeros(shape = (1, ydim, xdim))
            lwave = zeros(shape = (1, ydim, xdim))
            hwave = zeros(shape = (1, ydim, xdim))
            z0wave = zeros(shape = (1, ydim, xdim))
            twave[0,:,:] = waveperiod(wind_velocity)
            lwave[0,:,:] = wavelength(wind_velocity)
            hwave[0,:,:] = waveheight(wind_velocity)
            z0wave[0,:,:] = (0.0185/9.81) * Wu_ust(wind_velocity) ** 2 
            file_in.close()
            file_out = Dataset(file_list[i], 'a', format='NETCDF3_CLASSIC')
            t = file_out.dimensions['Time']
            x = file_out.dimensions['west_east']
            y = file_out.dimensions['south_north']

            vars = []
            for var in file_in.variables:
                vars.append(var)
            if "LWAVEP" in vars:
                file_in.variables['LWAVEP'][0,:,:] = lwave
                file_in.variables['TWAVEP'][0,:,:] = twave
                file_in.variables['HWAVEP'][0,:,:] = hwave
                file_in.variables['Z0WAVE'][0,:,:] = z0wave
            else:
                lWAVEP = file_out.createVariable('LWAVEP', 'f8', ('Time', 'south_north', 'west_east',))
                lWAVEP.FieldType = 104
                lWAVEP.MemoryOrder = "XY"
                lWAVEP.units = 'm'
                lWAVEP.description = 'PEAK WAVELENGTH'
                lWAVEP.stagger = "M"
                lWAVEP.sr_x = 1
                lWAVEP.sr_y = 1
                lWAVEP[:] = lwave

                tWAVEP = file_out.createVariable('TWAVEP', 'f8', ('Time', 'south_north', 'west_east',))
                tWAVEP.FieldType = 104
                tWAVEP.MemoryOrder = "XY"
                tWAVEP.units = 'sec'
                tWAVEP.description = 'PEAK WAVE PERIOD'
                tWAVEP.stagger = "M"
                tWAVEP.sr_x = 1
                tWAVEP.sr_y = 1
                tWAVEP[:] = twave
                
                hWAVEP = file_out.createVariable('HWAVEP', 'f8', ('Time', 'south_north', 'west_east',))
                hWAVEP.FieldType = 104
                hWAVEP.MemoryOrder = "XY"
                hWAVEP.units = 'm'
                hWAVEP.description = 'SIGNIFICANT WAVE HEIGHT'
                hWAVEP.stagger = "M"
                hWAVEP.sr_x = 1
                hWAVEP.sr_y = 1
                hWAVEP[:] = hwave           

                Z0wave = file_out.createVariable('Z0WAVE', 'f8', ('Time', 'south_north', 'west_east',))
                Z0wave.FieldType = 104
                Z0wave.MemoryOrder = "XY"
                Z0wave.units = 'm'
                Z0wave.description = 'SEA SURFACE ROUGHNESS - WAVES'
                Z0wave.stagger = "M"
                Z0wave.sr_x = 1
                Z0wave.sr_y = 1
                Z0wave[:] = z0wave          
            file_out.close()

if __name__ == "__main__":
    main()
  

